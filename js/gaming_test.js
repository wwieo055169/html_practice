function sendtxt() {
    var sendbox = document.getElementById("send");
    var resultbox = document.getElementById("result");

    resultbox.innerHTML += (sendbox.value + "\n");
    sendbox.value = "";
}

function sendtxt1() {
    var sendbox = document.getElementById("send1");
    var resultbox = document.getElementById("result1");

    resultbox.innerHTML += (sendbox.value + "\n");
    sendbox.value = "";
}

function checkdice() {
    var dice = document.getElementById("dice");
    var resultbox = document.getElementById("result");

    if (dice.value == "1d4")
        resultbox.innerHTML += ("1d4 => " + (Math.floor(Math.random() * 4 + 1)) + "\n");
    else if (dice.value == "1d6")
        resultbox.innerHTML += ("1d6 => " + (Math.floor(Math.random() * 6 + 1)) + "\n");
    else if (dice.value == "1d8")
        resultbox.innerHTML += ("1d8 => " + (Math.floor(Math.random() * 8 + 1)) + "\n");
    else if (dice.value == "1d10")
        resultbox.innerHTML += ("1d10 => " + (Math.floor(Math.random() * 10 + 1)) + "\n");
    else if (dice.value == "1d12")
        resultbox.innerHTML += ("1d12 => " + (Math.floor(Math.random() * 12 + 1)) + "\n");
    else if (dice.value == "1d20")
        resultbox.innerHTML += ("1d20 => " + (Math.floor(Math.random() * 20 + 1)) + "\n");
    else if (dice.value == "1d100")
        resultbox.innerHTML += ("1d100 => " + (Math.floor(Math.random() * 100 + 1)) + "\n");
}