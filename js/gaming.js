function swToNotes(){
    document.getElementById("notes").style.display = "block";
    document.getElementById("secret").style.display = "none";
    document.getElementById("dice").style.display = "none";
    document.getElementById("music").style.display = "none";
    document.getElementById("diceS").style.display = "block";
    document.getElementById("diceE").style.display = "none";
}

function swToSecret(){
    document.getElementById("notes").style.display = "none";
    document.getElementById("secret").style.display = "block";
    document.getElementById("dice").style.display = "none";
    document.getElementById("music").style.display = "none";
    document.getElementById("diceS").style.display = "block";
    document.getElementById("diceE").style.display = "none";
}

function swToDice(){
    document.getElementById("notes").style.display = "none";
    document.getElementById("secret").style.display = "none";
    document.getElementById("dice").style.display = "block";
    document.getElementById("music").style.display = "none";
    document.getElementById("diceS").style.display = "block";
    document.getElementById("diceE").style.display = "none";
}

function swToMusic(){
    document.getElementById("notes").style.display = "none";
    document.getElementById("secret").style.display = "none";
    document.getElementById("dice").style.display = "none";
    document.getElementById("music").style.display = "block";
    document.getElementById("diceS").style.display = "block";
    document.getElementById("diceE").style.display = "none";
}

function opendiceE(){
    document.getElementById("diceS").style.display = "none";
    document.getElementById("diceE").style.display = "block";
}

function opendiceS(){
    document.getElementById("diceS").style.display = "block";
    document.getElementById("diceE").style.display = "none";
}