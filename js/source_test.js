function SWrole() {
    document.getElementById("role").style.display = "block";
    document.getElementById("background").style.display = "none";
    document.getElementById("object").style.display = "none";
}

function SWbackground() {
    document.getElementById("role").style.display = "none";
    document.getElementById("background").style.display = "block";
    document.getElementById("object").style.display = "none";
}

function SWobject() {
    document.getElementById("role").style.display = "none";
    document.getElementById("background").style.display = "none";
    document.getElementById("object").style.display = "block";
}