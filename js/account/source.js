function SWtoBG() {
    document.getElementById("background").style.display = "block";
    document.getElementById("object").style.display = "none";
    document.getElementById("role").style.display = "none";
}

function SWtoObj() {
    document.getElementById("background").style.display = "none";
    document.getElementById("object").style.display = "block";
    document.getElementById("role").style.display = "none";
}

function SWtoRole() {
    document.getElementById("background").style.display = "none";
    document.getElementById("object").style.display = "none";
    document.getElementById("role").style.display = "block";
}