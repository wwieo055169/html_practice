addRoleDiv();

function addRoleDiv(obj) {
    var el = document.getElementById('role');
    while (el.firstChild) el.removeChild(el.firstChild);

    for (var i = 1; i <= 32; i++) {
        var div = document.createElement("div");
        div.style.width = "12.5%";
        div.style.height = "20vh";
        div.style.background = "gray";
        div.style.color = "black";
        div.innerHTML = "素材" + i.toString();
        div.style.display = "inline-block";
        div.style.borderStyle = "solid";
        div.style.padding = "0";
        div.style.margin = "0";

        var image = document.createElement('img');
        image.id = i.toString()
        image.src = "#";

        image.style.maxHeight = "80%";
        image.style.maxWidth = "100%";
        image.style.margin = "0 auto";
        image.style.display = "block";
        div.appendChild(image);

        document.getElementById("role").appendChild(div);
    }
}

function addBackgroundDiv(obj) {
    var el = document.getElementById('background');
    while (el.firstChild) el.removeChild(el.firstChild);

    for (var i = 1; i <= 32; i++) {
        var div = document.createElement("div");
        div.style.width = "12.5%";
        div.style.height = "20vh";
        div.style.background = "skyblue";
        div.style.color = "gray";
        div.innerHTML = "素材" + i.toString();
        div.style.display = "inline-block";
        div.style.borderStyle = "solid";
        div.style.padding = "0";
        div.style.margin = "0";
        div.id = i;
        document.getElementById("background").appendChild(div);
    }
}

function addObjectDiv(obj) {
    var el = document.getElementById('object');
    while (el.firstChild) el.removeChild(el.firstChild);

    for (var i = 1; i <= 32; i++) {
        var div = document.createElement("div");
        div.style.width = "12.5%";
        div.style.height = "20vh";
        div.style.background = "brown";
        div.style.color = "black";
        div.innerHTML = "素材" + i.toString();
        div.style.display = "inline-block";
        div.style.borderStyle = "solid";
        div.style.padding = "0";
        div.style.margin = "0";
        div.id = i;
        document.getElementById("object").appendChild(div);
    }
}