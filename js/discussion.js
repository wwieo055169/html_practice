function swChat(){
    document.getElementById("chatT").style.display = "block";
    document.getElementById("chatF").style.display = "none";
    document.getElementById("chat").style.display = "block";
    document.getElementById("chatD").style.display = "block";
    document.getElementById("chatH").style.display = "block";

    document.getElementById("reportT").style.display = "none";
    document.getElementById("reportF").style.display = "block";
    document.getElementById("report").style.display = "none";

    document.getElementById("scriptT").style.display = "none";
    document.getElementById("scriptF").style.display = "block";
    document.getElementById("script").style.display = "none";

    document.getElementById("crewT").style.display = "none";
    document.getElementById("crewF").style.display = "block";
    document.getElementById("crew").style.display = "none";

    document.getElementById("sourceT").style.display = "none";
    document.getElementById("sourceF").style.display = "block";
    document.getElementById("source").style.display = "none";
}

function swReport(){
    document.getElementById("reportT").style.display = "block";
    document.getElementById("reportF").style.display = "none";
    document.getElementById("report").style.display = "block";
    document.getElementById("chatD").style.display = "none";
    document.getElementById("chatH").style.display = "none";

    document.getElementById("chat").style.display = "none";
    document.getElementById("chatF").style.display = "block";
    document.getElementById("chatT").style.display = "none";

    document.getElementById("scriptT").style.display = "none";
    document.getElementById("scriptF").style.display = "block";
    document.getElementById("script").style.display = "none";

    document.getElementById("crewT").style.display = "none";
    document.getElementById("crewF").style.display = "block";
    document.getElementById("crew").style.display = "none";

    document.getElementById("sourceT").style.display = "none";
    document.getElementById("sourceF").style.display = "block";
    document.getElementById("source").style.display = "none";
}

function swScript(){
    document.getElementById("chatF").style.display = "block";
    document.getElementById("chatT").style.display = "none";
    document.getElementById("chat").style.display = "none";
    document.getElementById("chatD").style.display = "none";
    document.getElementById("chatH").style.display = "none";

    document.getElementById("reportT").style.display = "none";
    document.getElementById("reportF").style.display = "block";
    document.getElementById("report").style.display = "none";

    document.getElementById("scriptT").style.display = "block";
    document.getElementById("scriptF").style.display = "none";
    document.getElementById("script").style.display = "block";

    document.getElementById("crewT").style.display = "none";
    document.getElementById("crewF").style.display = "block";
    document.getElementById("crew").style.display = "none";

    document.getElementById("sourceT").style.display = "none";
    document.getElementById("sourceF").style.display = "block";
    document.getElementById("source").style.display = "none";
}

function swCrew(){
 
    document.getElementById("chatT").style.display = "none";
    document.getElementById("chatF").style.display = "block";
    document.getElementById("chat").style.display = "none";
    document.getElementById("chatD").style.display = "none";
    document.getElementById("chatH").style.display = "none";
    
    document.getElementById("reportT").style.display = "none";
    document.getElementById("reportF").style.display = "block";
    document.getElementById("report").style.display = "none";

    document.getElementById("scriptT").style.display = "none";
    document.getElementById("scriptF").style.display = "block";
    document.getElementById("script").style.display = "none";

    document.getElementById("crewT").style.display = "block";
    document.getElementById("crewF").style.display = "none";
    document.getElementById("crew").style.display = "block";

    document.getElementById("sourceT").style.display = "none";
    document.getElementById("sourceF").style.display = "block";
    document.getElementById("source").style.display = "none";
   
}

function swSource(){
 
    document.getElementById("chatT").style.display = "none";
    document.getElementById("chatF").style.display = "block";
    document.getElementById("chat").style.display = "none";
    document.getElementById("chatD").style.display = "none";
    document.getElementById("chatH").style.display = "none";
    
    document.getElementById("reportT").style.display = "none";
    document.getElementById("reportF").style.display = "block";
    document.getElementById("report").style.display = "none";

    document.getElementById("scriptT").style.display = "none";
    document.getElementById("scriptF").style.display = "block";
    document.getElementById("script").style.display = "none";

    document.getElementById("crewT").style.display = "none";
    document.getElementById("crewF").style.display = "block";
    document.getElementById("crew").style.display = "none";

    document.getElementById("sourceT").style.display = "block";
    document.getElementById("sourceF").style.display = "none";
    document.getElementById("source").style.display = "block";
   
}